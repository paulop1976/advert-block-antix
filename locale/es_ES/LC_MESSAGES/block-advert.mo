��          \      �       �   K   �           *     J     [  y   z  �  �  �  �  Y   L     �  '   �  &   �     
  �   )  �  �                                       <b> Failed </b> \n\n	antiX advert blocker must be run as root or with sudo  Choose what to block Loading  blocklist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites can't connect to this PC.\nYou can choose to block ads, malware, pornography, gambling, fakenews and social media\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:10+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2022
Language-Team: Spanish (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 <b> Falló </b> \n\n	El bloqueador de anuncios antiX debe ejecutarse como root o con sudo Elegir qué bloquear Cargando lista de bloqueo desde $domain No se ha seleccionado ningún elemento Restaurar /etc/hosts original. Exito -- su configuración se ha cambiado.\n\nSu archivo de hosts se ha actualizado.\nReinicie su navegador para visualizar los cambios. La herramienta <b>$title</b> añade cosas a su archivo /etc/hosts, de modo que\nmuchos servidores de publicidad y sitios web no puedan conectarse a este PC.\nPuede elegir bloquear anuncios, malware, pornografía, juegos de azar, fakenews y redes sociales.\nBloquear los servidores de publicidad protege su privacidad, le ahorra ancho de banda \nmejora la velocidad de navegación y hace que Internet sea mucho menos molesto en general.\n\n¿Quiere continuar? 